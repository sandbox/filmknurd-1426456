#!/bin/bash
###############################################################################
# Name:		install.sh
# Purpose:	Install and configure the build tools scripts
###############################################################################

TOOLS_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# TODO: Setup a .dbtools configuration file

# Setup an alias to the primary build script
$(sudo ln -s $TOOLS_DIR/drupal_build.sh /usr/local/bin/dbt)