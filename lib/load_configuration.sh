#!/bin/bash
###############################################################################
# Name:		load_configuration.sh
# Purpose:	Source the global and project configuration files
###############################################################################

function load_configuration() {
	. $HOME/dbt.cnf
	DIR=$(pwd)

	if [[ -f $DIR/project.cnf ]]; then
		. $DIR/project.cnf
	else
		echo "ERROR: Project configuration file (project.cnf) doesn't exist," 
		echo "or you are not executing this script from the project root."

		exit 1
	fi
}
