#!/bin/bash
###############################################################################
# Name:		drupal_build.sh
# Purpose:	Build a drupal distribution
###############################################################################

# Source the configuration files
# First, figure out where this script is located regardless of where it is 
# called. Then, call the load configuration script relative to the path
# of this script.
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done
TOOLS_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

. $TOOLS_DIR/lib/load_configuration.sh

load_configuration
